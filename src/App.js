import React, { Component } from 'react';
import './App.css';
import Gallery from './components/gallery.js';
import GoogleMapsContainer from './components/maps.js';


class App extends Component {
  render() {
    
    const data = this.props.data;

    const arreglo = ["hola","mundo","!"];


    const namesList = data.map((name,index) => {
     // console.log(name.property.name, name.property.phone)

     return (
       //<li key={name.property.id}>{name.property.name}</li>
       <div className="panel panel-default col-md-6 col-md-offset-3">
       <div className="panel-body text-center">

           {/* //Nombre de la propiedad = Calle + Numero exterior + Numero interior */}
         <h4>{name.property.address.street} #{name.property.address.number_ext} Interior {name.property.address.number_int}</h4> 
         <div className="col-md-6">
         <img src={name.property.img} className="img-responsive" alt="Responsive image"></img>
         </div>
         <div className="col-md-6">
         <h4>Descripción: </h4>
         <p className="descripcion">{name.property.description}</p>
         </div>
       </div>
       <div className="list-group-horizontal">
         <a key={name.property.id} href="#" className="list-group-item" id="button1">
          Precio: $ {name.property.price}
         </a>
         <a href="#" className="list-group-item " id="button2" data-toggle="modal" data-target={"#"+name.property.id+"modalDetalles"}>
           Consultar Detalle
         </a>
       </div>
    {/* //Modal*/}  
    <div className="modal fade" id={name.property.id+"modalDetalles"} role="dialog">
    <div className="modal-dialog">
    
      {/* //Contenido delModal*/} 
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal">&times;</button>
          <h4 className="modal-title"> {name.property.address.street} #{name.property.address.number_ext} Interior {name.property.address.number_int}</h4>
        </div>
        <div className="modal-body">


        <Gallery  images={name.property.images} index={index} />

        <h4>Dirección: {name.property.address.street} {name.property.address.number_ext} {name.property.address.number_int} {name.property.address.city} {name.property.address.state}</h4>
        <div>
        <h4>Costo de la propiedad: ${name.property.price}</h4>
        </div>
        <h4 className="descripcion">Descripción de la propiedad: {name.property.description}</h4>
        <GoogleMapsContainer latitud={name.property.coords.latitud} longitud={name.property.coords.longitud} nombre={name.property.address.street}/>
           <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-default close" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
     </div>

       
     )

    })


    return (
      <div className="App">
      
<h1 className="text-center">Listado de Propiedades</h1>

      {namesList}
      

      </div>
    );
  }
}

export default App;
