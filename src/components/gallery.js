import React, { Component } from 'react';
import '../App.css';

class Gallery extends Component {

    constructor(props)
    {
        super(props);
    }

  render() {

    return(
    <div className="container-fluid">

    <h2>Galería de imágenes</h2>  
    <div id={this.props.index} className="carousel slide" data-ride="carousel">
      <ol className="carousel-indicators">

        {
            this.props.images.map( (img, index) => {
                return(
                   
                    
                    <li data-target={"#"+this.props.index} data-slide-to={index} className={index==0?'active':''}></li> 
                )
            })
        }
      </ol>
  
      <div className="carousel-inner">

      {
            this.props.images.map( (url, index) => {
                return(
                   
                    <div className= {index==0?'item active':'item'}>
                        <img src={url} className="image-gallery" alt="img"/>
                    </div>
                )
            })
        }
  
        
      </div>
  
      <a className="left carousel-control" href={"#"+this.props.index} data-slide="prev">
        <span className="glyphicon glyphicon-chevron-left"></span>
        <span className="sr-only">Atrás</span>
      </a>
      <a className="right carousel-control" href={"#"+this.props.index} data-slide="next">
        <span className="glyphicon glyphicon-chevron-right"></span>
        <span className="sr-only">Siguiente</span>
      </a>
    </div>
  </div>
    )
  }
}

export default Gallery;