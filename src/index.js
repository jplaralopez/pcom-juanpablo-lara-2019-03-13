import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import data from './data/data';
import * as serviceWorker from './serviceWorker';

ReactDOM.render
    (<App data={data} />, 
    document.getElementById('root'));


serviceWorker.unregister();
